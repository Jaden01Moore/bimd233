var state = "IDLE";
var cmd = "";

function getUserInput() {
  let cmd = prompt("State: " + state, "next");
  if (cmd !== null){
    cmd = cmd.toLowerCase();
  }
  return cmd;
}

do {
  switch (state) {
    case "IDLE":
      {
        if (cmd === "run") {
          state = "S1";
        }
      }
      break;
    //State S1 Behavior
    case "S1":
      {
        if (cmd === "next") {
          state = "S2";
        } else if (cmd === "skip") {
          state = "S3";
        } else if (cmd === "prev") {
          state = "S4";
        }
      }
      break;
    //State S2 Behavior
    case "S2":
      {
        if (cmd === "next") {
          state = "S3";
        } else if (cmd === "skip") {
          state = "S4";
        } else if (cmd === "prev") {
          state = "S1";
        }
      }
      break;
    case "S3":
      {
        if (cmd === "next") {
          state = "S4";
        } else if (cmd === "skip") {
          state = "S5";
        } else if (cmd === "prev") {
          state = "S2";
        }
      }
      break;
    case "S4":
      {
        if (cmd === "next") {
          state = "S1";
        } else if (cmd === "skip") {
          state = "S2";
        } else if (cmd === "prev") {
          state = "S3";
        }
      }
      break;
  }
  cmd = getUserInput();
} while (cmd != "exit");
