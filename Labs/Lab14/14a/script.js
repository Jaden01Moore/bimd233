var wx_data = [
    {day: "fri", hi: 82, lo: 55},
    {day: "sat", hi: 75, lo: 52},
    {day: "sun", hi: 69, lo: 52},
    {day: "mon", hi: 69, lo: 48},
    {day: "tue", hi: 68, lo: 51},
  ];
  var str = "";
  for( var i = 0; i < wx_data.length; i++){
      //console.log(wx_data[i]);
      str += myHTMLRow(wx_data[i]);
      //console.log(str);
  }

  var el = document.getElementById("w-data");
  el.innerHTML = str;

  function myHTMLRow(dataLine){
      let str = "<tr>";
      str += "<td>" + dataLine.day + "</td>";
      str += "<td>" + dataLine.hi + "</td>";
      str += "<td>" + dataLine.lo + "</td>";
      str += "<td>" + (dataLine.hi + dataLine.lo)/2 + "</td>"
      str += "</tr>"
    return str;
  }

  var str2 = wx_data.reduce(getWeekAvg, 0);
  document.getElementById("week-avg").innerHTML = str2;

  function getWeekAvg(total, currentValue, i, arr){
      //console.log(total + "||||" + currentValue);
      var dayAvg = (currentValue.hi + currentValue.lo) / 2;
      console.log(total + "||" + dayAvg);
      total += dayAvg;
      if(i == arr.length - 1){
          total = total / arr.length
      }
      return total;


  }
  