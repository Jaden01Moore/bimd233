var airline;
var number;
var origin;
var destination;
var departureTime;
var arrivalTime;
var arrivalGate;

var data = [
    {airline: "ALASKA AIRLINES", number: "ASA1002", origin: "REAGAN NATIONAL - DCA", destination: "SAN FRANCISCO - SFO", departureTime: new Date("November 22, 2020 08:34:00"), arrivalTime: new Date("November 22, 2020 11:26:00"), arrivalGate: "D6"},
    {airline: "ALASKA AIRLINES", number: "ASA1012", origin: "SEATTLE-TACOMA - SEA", destination: "MINNEAPOLIS-ST PAUL - MSP", departureTime: new Date("November 22, 2020 06:38:00"), arrivalTime: new Date("November 23, 11:26:00"), arrivalGate: "E1"},
    {airline: "ALASKA AIRLINES", number: "ASA111", origin: "SEATTLE-TACOMA - SEA", destination: "ANCHORAGE INT - ANC", departureTime: new Date("November 22, 2020 06:37:00"), arrivalTime: new Date("November 22, 2020 09:52:00"), arrivalGate: "C1"},
    {airline: "ALASKA AIRLINES", number: "ASA1116", origin: "SEATTLE-TACOMA - SEA", destination: "MCCARRAN INT - LAS", departureTime: new Date("November 22, 2020 08:17:00"), arrivalTime: new Date("November 22, 2020 10:39:00"), arrivalGate: "D33"},
    {airline: "ALASKA AIRLINES", number: "ASA1146", origin: "SEATTLE-TACOMA - SEA", destination: "AUSTIN-BERGSTROM INT - AUS", departureTime: new Date("November 22, 2020 05:58:00"), arrivalTime: new Date("November 22, 2020 11:43:00"), arrivalGate: "D33"}
]

var test1 = data[0].arrivalTime.getTime() - data[0].departureTime.getTime();
var test2 = data[1].arrivalTime.getTime() - data[1].departureTime.getTime();
var test3 = data[2].arrivalTime.getTime() - data[2].departureTime.getTime();
var test4 = data[3].arrivalTime.getTime() - data[3].departureTime.getTime();
var test5 = data[4].arrivalTime.getTime() - data[4].departureTime.getTime();
var total = [test1, test2, test3, test4, test5]


var str = "";
for(var i = 0; i < data.length; i++){
    str += myHTMLRow(data[i], total[i]);
}

var el = document.getElementById("date");
el.innerHTML = str;

function myHTMLRow(data, time){
    let str = "<tr>";
    str += "<td>" + data.airline + "</td>";
    str += "<td>" + data.number + "</td>";
    str += "<td>" + data.origin + "</td>";
    str += "<td>" + data.destination + "</td>";
    str += "<td>" + data.departureTime + "</td>";
    str += "<td>" + data.arrivalTime + "</td>";
    str += "<td>" + data.arrivalGate + "</td>";
    str += "<td>" + hh_mm_ss(time) + "</td>";
    str += "</tr>"
  return str;
}





function zeropad(num, size) {
    var s = "000000000" + num;
    return s.substr(s.length - size);
  }
  
  // Compute the integer hours, minutes, seconds given a time in milliseconds
  // Input: time_ms = time in milliseconds
  // Output: array of integers representing hours, min, sec
  function hh_mm_ss(time_ms) {
    let hh_re = time_ms / 1000.0 / 60.0 / 60.0;
    let hh = Math.floor(hh_re);
    let mm_re = (hh_re - hh) * 60.0;
    let mm = Math.floor(mm_re);
    let ss_re = (mm_re - mm) * 60.0;
    let ss = Math.floor(ss_re);
  
    hh = zeropad(hh, 2);
    mm = zeropad(mm, 2);
    ss = zeropad(ss, 2);
  
    return hh + ":" + mm + ":" + ss;
  }
  
  console.log("Result: " + hh_mm_ss(5132160));
  console.log("Result: " + hh_mm_ss(15132160));
  console.log("Result: " + hh_mm_ss(51421234));

  console.log("my results: " + hh_mm_ss(test))
  