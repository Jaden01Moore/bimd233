$(document).ready(function () {
  // --------- jQuery Data Section ---------
  var book1 = {
    title: "Upheaval",
    author: "Jared Diamond",
    image: "Image1.jpeg"
  };
  var book2 = {
    title: "Nine Pints",
    author: "Rose George",
    image: "Image2.jpeg"
  };
  var book3 = {
    title: "The Future of Capitalism",
    author: "Paul Collier",
    image: "Image3.jpeg"
  };
  var book4 = {
    title: "Presidents of War",
    author: "Michael Beschloss",
    image: "Image4.jpeg"
  };
  var book5 = {
    title: "A Gentleman in Moscow",
    author: "Amor Towles",
    image: "Image5.jpeg"
  };

  var books = new Array();
  books.push(book1);
  books.push(book2);
  books.push(book3);
  books.push(book4);
  books.push(book5);

  var img_ref = {
    url:
      "https://i.insider.com/5a8de646391d948e008b4795?width=1300&format=jpeg&auto=webp",
    src: "https://bit.ly/338TQE6",
    alt: "Bill Gates",
    height: 150,
    width: 250
  };

  var reference = {
    url:
      "https://www.businessinsider.com/bill-gates-book-recommendations-summer-2019-5",
    src: "http://usat.ly/20hirO3",
    alt: "Gates Books",
    text: "BG:5 Books for Summer 2019"
  };
  // --------- jQuery Data Section ---------








  // --------- jQuery Code Section ---------

  // apply bootstrap panel classes
  $("img").attr(img_ref);
  $("a").attr(reference);
  $('ol').addClass("list-group");
  $('li').addClass("list-group-item");

  $('li').each(function (i) {
    let str = '"' + books[i].title + '"' + ' by '+ books[i].author;
    let imageUrl = "image/" + books[i].image;
    let image = '<img src="'+imageUrl+'">'+str;
    $(this).html(image);
});


  $('li').each(function (i) {
    if(i%2!=0){
      $(this).addClass("even");
    }else{
      $(this).addClass("odd");
    }
  });
  // --------- jQuery Code Section ---------
});