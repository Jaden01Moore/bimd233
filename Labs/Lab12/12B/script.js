var arrMake; ["Camaro", "Mustang", "Charger", "Challenger", "Aventador"];
var arrModel;["Chevrolet", "Ford", "Dodge", "Dodge", "Lamborghini"];
var arrYear;[2011, 2013, 2021, 2020, 2020];
var arrPrice;[30000, 23000, 30000, 28000, 417000];
var arrCarList;[arrMake, arrModel, arrPrice, arrPrice];

var data=[
    {arrMake: "Chevrolet", arrModel: "Camaro", arrYear: "2011", arrPrice: "30000"},
    {arrMake: "Ford", arrModel: "Mustang", arrYear: "2013", arrPrice: "23000"},
    {arrMake: "Dodge", arrModel: "Charger", arrYear: "2020", arrPrice: "30000"},
    {arrMake: "Dodge", arrModel: "Challenger", arrYear: "2020", arrPrice: "28000"},
    {arrMake: "Lamborghini", arrModel: "Aventador", arrYear: "2020", arrPrice: "417000"}
]

var str = "";
for(var i = 0; i < data.length; i++){
    str += myHTMLRow(data[i]);
}

var el = document.getElementById("date");
el.innerHTML = str;

function myHTMLRow(data){
    let str = "<tr>";
    str += "<td>" + data.arrMake + "</td>";
    str += "<td>" + data.arrModel + "</td>";
    str += "<td>" + data.arrYear + "</td>";
    str += "<td>" + data.arrPrice + "</td>";
  return str;
}