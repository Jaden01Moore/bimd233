function calcCircleGeometries(radius) {
    const pi = Math.PI;
    var area = pi * radius * radius;
    var circumference = 2 * pi * radius;
    var diameter = 2 * radius;
    var geometries = [area, circumference, diameter];
    return geometries;
  }
  
  function geoRow(r){
    let g = calcCircleGeometries(r);
    let str = "";
    str += "<tr><td>" + r.toFixed(2) + "</td>";
    for(var i = 0; i < g.length; i++){
      str += "<td>"+g[i].toFixed(4)+"</td>"; 
    }
    str += "</tr>";
    return str;
  }
  
  var el = document.getElementById('geo-body');
  el.innerHTML = "";
  
  var i;
  for(i = 0; i < 3; i++){
    let r = 100*Math.random();
    el.innerHTML += geoRow(r);
  }