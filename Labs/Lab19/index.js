$(document).ready(function() {
  const base_url="https://api.weather.gov/stations/";
  const endpoint="/observations/latest";

  // weather update button click
  $('#getwx').on('click', function(e) {
    var mystation = $('input').val();
    var myurl = base_url + mystation + endpoint;
    $('input#my-url').val(myurl);
    
    // clear out any previous data
    $('ul li').each(function() {
      $('.list-group').empty();
    });
      
    console.log("Cleared Elements of UL");
    
    // execute AJAX call to get and render data
    $.ajax({
      url: myurl,
      dataType: "json",
      success: function(data) {
        try {
          var tempC= data['properties']['temperature'].value.toFixed(1);
          var tempF = (tempC * 9/5 + 32).toFixed(1);
          
          // get wind info and convert m/s to kts
          var windDirection = data['properties']['windDirection'].value.toFixed(0);
          var windSpeed = (((data['properties']['windSpeed'].value) / 3.6) * 1.94384).toFixed(1);
        } catch (error) {
          $('textarea').val(error);
        }
        
        // uncomment this if you want to dump full JSON to textarea
        var myJSON = JSON.stringify(data);
      
        var str = "<li>Current temperature: " + tempC +"C " + tempF+"F"+"</li>";
        
        $('ul').append(str);
        $('ul li:last').attr('class', 'list-group-item');

        // add additional code here for the Wind direction, speed, weather contitions and icon image
        str = "<li>Wind direction: " + windDirection + "*, wind speed: " + windSpeed + "kts" + "</li>";

        $('ul').append(str);
        $('ul li:last').attr('class', 'list-group-item');

        //weather image
        var weatherImage = data['properties']['icon'];
        var textDescription = data['properties']['textDescription'];
        str = '<li> Weather condition: <img src="' + weatherImage + '">' + textDescription + '</li>';
        $('ul').append(str);
        $('ul li:last').attr('class', 'list-group-item');
      }
    });
  });
});