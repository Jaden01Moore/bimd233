var arrStocks = [];

function stock(companyName, marketCap, sales, profit, numberOfEmployees){
    var tempObj = {};
    tempObj.CN = companyName;
    tempObj.MC = marketCap;
    tempObj.S = sales;
    tempObj.P = profit;
    tempObj.NE = numberOfEmployees;
    return tempObj;
}

arrStocks.push(stock("Microsoft", "$381.7 B", "86.8 B", "22.1 B", "128,000"));
arrStocks.push(stock("Symetra Financial", "$2.7 B", "2.2 B", "254.4 M", "1,400"));
arrStocks.push(stock("Micron Technology", "$37.6 B", "16.4 B", "3.0 B", "30,400"));
arrStocks.push(stock("F5 Networks", "$9.5 B", "1.7 B", "311.2 M", "3,834"));
arrStocks.push(stock("Expedia", "$10.8 B", "5.8 B", "398.1 M", "18,210"));
arrStocks.push(stock("Nautilus", "$476 M", "274.4 M", "18.8 M", "340"));
arrStocks.push(stock("Heritage Financial", "$531 M", "137.6 M", "21 M", "748"));
arrStocks.push(stock("Cascade Microtech", "$239 M", "136 M", "9.9 M", "449"));
arrStocks.push(stock("Nike", "$83.1 B", "27.8 B", "2.7 B", "56,500"));
arrStocks.push(stock("Alaska Air Group", "$7.9 B", "5.4 B", "605 M", "13,952"));

arrStocks.forEach(dummy);
function dummy(item, i, arr){
    console.log(item,i);

}

arrStocks.forEach(myHTMLcode);
function myHTMLcode(item, i, arr){
    var domRoot = document.querySelector("tbody");
    var row = document.createElement("tr");
    row.innerHTML = "<td>" + item.CN + "</td>"
    row.innerHTML += "<td>" + item.MC + "</td>"
    row.innerHTML += "<td>" + item.S + "</td>"
    row.innerHTML += "<td>" + item.P + "</td>"
    row.innerHTML += "<td>" + item.NE + "</td>"
    domRoot.appendChild(row);
}