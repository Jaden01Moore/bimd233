var pac12 = {};
pac12.logo = [];
pac12.school = [];
pac12.conference = [];
pac12.overallRecord = [];
pac12.lastGameOutcome = [];
pac12.nextOpponent = [];
pac12.nextGame = [];
pac12.numSchools = 6;

pac12.logo.push(
    "https://cdn.bleacherreport.net/images/team_logos/328x328/washington_huskies_football.png", 
    "https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Oregon_Ducks_logo.svg/1200px-Oregon_Ducks_logo.svg.png", 
    "https://upload.wikimedia.org/wikipedia/en/thumb/1/1b/Oregon_State_Beavers_logo.svg/1200px-Oregon_State_Beavers_logo.svg.png",
    "https://upload.wikimedia.org/wikipedia/en/thumb/0/07/Washington_State_Cougars_logo.svg/1200px-Washington_State_Cougars_logo.svg.png",
    "https://upload.wikimedia.org/wikipedia/commons/thumb/4/4b/Stanford_Cardinal_logo.svg/118px-Stanford_Cardinal_logo.svg.png",
    "https://cdn.bleacherreport.net/images/team_logos/328x328/cal_bears_football.png"
    );
pac12.school.push("Washington", "Oregon", "Oregon State", "Washington State", "Stanford" , "California");
pac12.conference.push("3-0", "3-1", "2-2", "1-1", "1-2", "0-3");
pac12.overallRecord.push("3-0", "3-1", "2-2", "1-1", "1-2", "0-3");
pac12.lastGameOutcome.push("W 24-21 UTAH", "L 38-41 ORST", "W 41-38 ORE", "CANCELLED WASH", "W 24-23 CAL", "L 23-24 STAN");
pac12.nextOpponent.push("https://upload.wikimedia.org/wikipedia/commons/thumb/4/4b/Stanford_Cardinal_logo.svg/118px-Stanford_Cardinal_logo.svg.png", 
"https://cdn.bleacherreport.net/images/team_logos/328x328/cal_bears_football.png",
"https://upload.wikimedia.org/wikipedia/commons/b/be/Utah_Utes_logo.svg", 
"https://upload.wikimedia.org/wikipedia/commons/9/94/USC_Trojans_logo.svg",
"https://cdn.bleacherreport.net/images/team_logos/328x328/washington_huskies_football.png",
"https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Oregon_Ducks_logo.svg/1200px-Oregon_Ducks_logo.svg.png"
);
pac12.nextGame.push("12/5 TBD", "12/5 TBD", "12/5 TBD", "12/4 FS1", "12/5 TBD", "12/5 TBD");

console.log(pac12);

function makeRowHTML(row) {
    let str = "";
    str += '<div class="row">';
    str += '<div class="col"><img src="' + pac12.logo[row] + '" width="50px"></div>';
    str += '<div class="col">' + pac12.school[row] + '</div>';
    str += '<div class="col">' + pac12.conference[row] + '</div>';
    str += '<div class="col">' + pac12.overallRecord[row] + '</div>';
    str += '<div class="col">' + pac12.lastGameOutcome[row] + '</div>';
    str += '<div class="col"><img src="' + pac12.nextOpponent[row] + '"width="25px"></div>';
    return str;
}

function GenGrid() {
    let el = document.getElementById("dataPrint");
    el.innerHTML = "";
    for(var i = 0; i < pac12.numSchools; i++){
        console.log(makeRowHTML(i));
        el.innerHTML += makeRowHTML(i);
    }
}

GenGrid();