$(document).ready(function () {
  $('li').css('margin', '10px');
  $('li').attr('id', 'uw');

  $("#p1 li").click(function () {
    console.log("$(this):" + $(this));
    $(this).fadeOut(2000, function () {
      console.log("fadeout complete!");
    });
  });


  $("#p2 li").click(function () {
    console.log("fadeIn" + $(this).text());
    $(this).fadeOut(0, function () {
      $(this).fadeIn(2000, function () {
        console.log("fadeIn complete!");
      });
    });
  });

  $("#p3 li").click(function () {
    console.log("fadeTo" + $(this).text());
    $(this).fadeTo(2000, 0.1, function () {
      console.log("FadeTo complete!");
    });
  });


  $('#p4 li').click(function () {
    console.log("fadeToggle" + $(this).text());
    $(this).fadeToggle(2000, function() {
      console.log("FadeToggle complete!");
    });
    });

  });
