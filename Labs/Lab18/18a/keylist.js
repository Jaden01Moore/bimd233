$(document).ready(function () {
  $("li").css("id", "uw");
  const states = ["idle", "gather", "process"];
  var state = states[0];
  var words = new Array();
  var ndx = 0;

  $("ul").on("mouseover", "li", function () {
    console.log("x:" + $(this).text());
    $(this).attr("id", "uw");
  });

  $("ul").on("mouseleave", "li", function () {
    $(this).attr("id", "uw-gold");
  });

  // reset button click
  var codeHTML = "";
  var str = "";
  $("button").on("click", function (e) {
    worde = [];
    codeHTML = "";
    str = "";
    $("table").empty();
    $("input").val("");
  });

  // keypress
  $("input").on("keypress", function (e) {
    var code = e.which;
    var arrlength = words.length;


    if (code == 13) {
      words.push(str);
      str = "";
      $("input").val("");
      $("table").append('<tr><td>' + words[arrlength] + '</td></tr>');


    } else {
      str += String.fromCharCode(code);
    }

    var char = String.fromCharCode(code);
    //console.log('key:' + code + '\tstate:' + state);

    switch (state) {
      // idle
      case "idle":
        state = states[0];
        break;

      // gather
      case "gather":
        state = states[1];
        break;

      // process
      case "process":
        state = states[2];
        break;

      default:
        break;
    }
  });
});
